console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:
	let first_name   = "John";
    let last_name    = "Smith";
    let age          = 30;
    let hobbies      = ["Biking",
                        "Mountain Climbing",
                        "Swimming"];
    let work_address = {houseNumber: "32",
                        street: "Washington",
                        city: "Lincoln",
                        state: "Nebraska"};
                        
    // String
    console.log("First Name: " + first_name);
    console.log("Last Name: " + last_name);
    
    // Number
    console.log("Age: " + age);
    
    // Array
    console.log("Hobbies:");
    console.log(hobbies);
    
    //Object
    console.log("Work Address:");
    console.log(work_address);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName    = "Steve Rogers";
	let name = fullName;
	console.log("My full name is: " + name);

	let currentAge         = 40;
	age = currentAge;
	console.log("My current age is: " + age);
	
	let friends     = [
	                   "Tony",
	                   "Bruce",
	                   "Thor",
	                   "Natasha",
	                   "Clint",
	                   "Nick"
	                  ];
	console.log("My Friends are: ")
	console.log(friends);

	let profile = {
		           username: "captain_america",
		           fullName: "Steve Rogers",
		           age: 40,
		           isActive: false,
                  };
	console.log("My Full Profile: ");
	console.log(profile);

	fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation = ["Arctic Ocean"];
	lastLocation[1] = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation[0]);